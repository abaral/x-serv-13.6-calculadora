# Calculadora para sumar y restar

def suma(numero1: int, numero2: int) -> int:
    """Suma los dos números que se le pasan y devuelve el resultado"""
    return numero1 + numero2


def resta(numero1: int, numero2: int) -> int:
    """Calcula la diferencia entre los dos números que se le pasan y devuelve el resultado"""
    return numero1 - numero2


num1: int = 1
num2: int = 2
num3: int = 3
num4: int = 4
num5: int = 5
num6: int = 6
num7: int = 7
num8: int = 8

print(num1, "+", num2, "=", suma(num1, num2))
print(num3, "+", num4, "=", suma(num3, num4))
print(num6, "-", num5, "=", resta(num6, num5))
print(num8, "-", num7, "=", resta(num8, num7))
